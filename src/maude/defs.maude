***(

    This file is part of SRAPlexil, a rewriting logic semantics of
    the Plan Execution Interchange Language -PLEXIL- with symbolic
    reachability analysis support modulo SMT theories

    Copyright (C) 2008-2012 Camilo Rocha, camilo.rocha@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
)

****                                                           Main definitions
****                                                       Author: Camilo Rocha

set include BOOL off .

--- Object attributes
fmod ATTRIB is
  sort AttribName .
  sort AttribValue .
endfm

view AttribName from TRIV to ATTRIB is
  sort Elt to AttribName .
endv

view AttribValue from TRIV to ATTRIB is
  sort Elt to AttribValue .
endv

--- Unqualified names
fmod NAME is
  sort Name .
endfm

view Name from TRIV to NAME is
  sort Elt to Name .
endv

--- Qualified names
fmod QUALIFIED-NAME is
  pr LIST{Name} * (sort NeList{Name} to NeQualName,
                   sort List{Name} to QualName,
                   op __ to _._) .
endfm

view NeQualName from TRIV to QUALIFIED-NAME is
  sort Elt to NeQualName .
endv

--- Node status
fmod STATUS is
  sort Status .
  op inactive : -> Status  [ctor format(! o)] .
  op waiting : -> Status   [ctor format(c! o)] .
  op executing : -> Status [ctor format(g! o)] .
  op finishing : -> Status [ctor format(m! o)] .
  op it-ended : -> Status  [ctor format(y! o)] .
  op failing : -> Status   [ctor format(r! o)] .
  op finished : -> Status  [ctor format(b! o)] .
endfm

--- Node outcome
fmod OUTCOME is
  sort FailFlag .
  ops parent pre inv : -> FailFlag .

  sort Outcome .
  op none    : -> Outcome [ctor] .
  op success : -> Outcome [ctor format(u o)] .
  op skipped : -> Outcome [ctor format(u o)] .
  op failure : -> Outcome [ctor format(u o)] .
  op fail    : FailFlag -> Outcome .
endfm

--- External variable type
fmod EXT-VAR-TYPE is
  sort ExtVarType .
  ops int boolean : -> ExtVarType .
endfm


--- Boolean and integer expressions
fmod EXPR is
  pr INT .
  pr QUALIFIED-NAME .
  pr STATUS .
  pr OUTCOME .
  pr TRUTH-VALUE .

  sort iExpr .

  --- Booleans and integers
  sort iBool iBoolCns iBoolVar .
  sort iInt iIntCns iIntVar .
  subsort iBoolCns iBoolVar < iBool .
  subsort iIntCns  iIntVar < iInt .
  subsorts iBool iInt < iExpr .

  --- Boolean and integer constants
  op c : Bool -> iBoolCns [ctor] .
  op c : Int -> iIntCns [ctor] .

  --- Boolean and integer variables
  op b : Nat -> iBoolVar [ctor] .
  op i : Nat -> iIntVar [ctor] . 

  --- Boolean expressions
  op bmem : NeQualName -> iBool .       --- binding for local Boolean variables
  op blookup : NeQualName -> iBool .    --- binding for external Boolean variables
  op ~_ : iBool -> iBool [prec 41] .
  ops _^_ _v_ : iBool iBool -> iBool [assoc comm prec 45] .
  op _->_ : iBool iBool -> iBool [prec 47] .
  ops _===_ _=//=_ : iBool iBool -> iBool [comm prec 50] .

  --- Integer expressions
  op imem : NeQualName -> iInt .        --- binding for local integer variables
  op ilookup : NeQualName -> iInt .     --- binding for external integer variables
  op -_ : iInt -> iInt [prec 31] .
  ops _+_ _*_ : iInt iInt -> iInt [assoc comm prec 35] .
  op _-_ : iInt iInt -> iInt .

  --- Relational expressions on integers
  ops _<=_ _<_ _>=_ _>_ : iInt iInt -> iBool [prec 37] .
  ops _===_ _=//=_ : iInt iInt -> iBool [comm prec 50] .

  --- Node expressions
  op anc-end-true  : NeQualName -> iBool .
  op anc-inv-false : NeQualName -> iBool .
  op is-executing  : NeQualName -> iBool .
  op is-waiting    : NeQualName -> iBool .
  op is-status     : NeQualName Status -> iBool .
  op is-outcome    : NeQualName Outcome -> iBool .
  op is-failure    : NeQualName -> iBool .
  op is-parent-failure : Outcome -> iBool .
  op children-waiting-or-finished : NeQualName -> iBool .
  op children-finished : NeQualName -> iBool .
endfm

--- Boolean expressions can instantiate parametric types
view iBool from TRIV to EXPR is
  sort Elt to iBool .
endv

--- expressions can instantiate parametric types
view iExpr from TRIV to EXPR is
  sort Elt to iExpr .
endv

--- lists of atomic Boolean and integer expressions, and expressions
fmod EXPR-LIST is
  pr EXPR .

  --- list of Boolean expressions
  sorts NeIExprList iExprList .
  subsorts iExpr < NeIExprList < iExprList .
  op nilel : -> iExprList [ctor] .
  op _;_ : NeIExprList NeIExprList -> NeIExprList [ctor assoc id: nilel] .
  op _;_ : iExprList iExprList -> iExprList [assoc id: nilel] .
endfm

--- Configuration of objects and messages, adapted and modified from
--- Maude's prelude
fmod CONFIG is
  pr MAP{AttribName,AttribValue} * (sort Entry{AttribName,AttribValue} to Attribute,
                                    sort Map{AttribName,AttribValue} to AttributeSet,
                                    op empty to none) .

  sorts Oid Cid Object Msg Configuration .
  subsort Object Msg < Configuration .
  op <_:_|_> : Oid Cid AttributeSet -> Object [ctor object prec 123 format(n d d d d d d d)] .
  op none : -> Configuration [ctor] .
  op __ : Configuration Configuration -> Configuration [ctor prec 125 assoc comm id: none] .
endfm

view Oid from TRIV to CONFIG is
  sort Elt to Oid .
endv

view Configuration from TRIV to CONFIG is
  sort Elt to Configuration .
endv

--- sets of object identifiers
fmod OID-SET is
  pr SET{Oid} * (sort NeSet{Oid} to NeOidSet,
                 sort Set{Oid} to OidSet,
                 op empty to mtoidset) .
endfm

view OidSet from TRIV to OID-SET is
  sort Elt to OidSet .
endv

--- Atributes of nodes
fmod PLX-CONFIG is
  inc CONFIG .
  pr QUALIFIED-NAME .
  pr STATUS .
  pr OUTCOME .
  pr EXPR .
  pr EXT-BOOL .
  pr OID-SET .
  pr EXPR-LIST .
  pr EXT-VAR-TYPE .

  var  AtS        : AttributeSet .
  vars Cnf Cnf'   : Configuration .
  vars C C'       : Cid .
  var  iE         : iExpr .
  var  Nm         : Name .
  vars NeQN NeQN' : NeQualName .
  var  O          : Oid .
  var  OS         : OidSet .
  var  Out        : Outcome .
  var  QN         : QualName .
  var  St         : Status .

  subsort QualName < Oid .                            --- Names are used as object identifiers
  --- classes
  ops list assg mem : -> Cid [ctor format(! o)] .     --- List, assignment, and memory nodes
  ops start end repeat skip : -> AttribName [ctor] .  --- Control conditions
  ops extvar        : -> Cid [ctor format(! o)] .     --- External variables

  --- attribute names
  ops pre post inv : -> AttribName [ctor] .           --- Check conditions
  ops status outcome : -> AttribName [ctor] .         --- State and outcome of execution
  ops parent : -> AttribName [ctor] .                 --- Parent 
  ops val init-val : -> AttribName [ctor] .           --- Current and initial value of memories
  op  set : NeQualName -> AttribName [ctor] .         --- Assignment to memories
  ops values constraints type : -> AttribName [ctor] .   --- List of values, constraints, and type associated to an external variable

  --- attribute values
  subsorts QualName Status Outcome iExprList ExtVarType < AttribValue .

  --- collects the name of the nodes in the given configuration
  op get-names  : Configuration -> OidSet .
  op $get-names : Configuration OidSet -> OidSet .
  eq get-names(Cnf)
   = $get-names(Cnf,mtoidset) .
  eq $get-names((none).Configuration,OS)
   = OS .
  eq $get-names((Cnf < O : C | AtS >),OS)
   = $get-names(Cnf,(OS,O)) .

  --- collects the children nodes with class mem
  op get-children-mem  : Configuration NeQualName -> Configuration .

  --- collects the children nodes with the given class
  op $get-children : Configuration NeQualName Cid Configuration -> Configuration .
  eq get-children-mem(Cnf,NeQN)
   = $get-children(Cnf,NeQN,mem,(none).Configuration) .
  eq $get-children((none).Configuration,NeQN,C,Cnf')
   = Cnf' .
  eq $get-children((Cnf < Nm . QN : C' | AtS >),NeQN,C,Cnf')
   = if QN == NeQN and-then C' == C
     then $get-children(Cnf,NeQN,C,(Cnf' < Nm . QN : C | AtS > ))
     else $get-children(Cnf,NeQN,C,Cnf')
     fi .
endfm

--- Evaluation of expressions in Plexil configurations
fmod EVALUATION is
  pr PLX-CONFIG .

  var  AtS        : AttributeSet .
  vars B B'       : Bool .
  var  Cls        : Cid .
  var  Cnf Cnf'   : Configuration .
  vars iB iB'     : iBool .
  var  iEL        : iExprList .
  vars iI iI'     : iInt .
  var  I I'       : Int .
  vars Nm Nm'     : Name .
  vars N N'       : Nat .
  vars NeQN NeQN' : NeQualName .
  var  Out Out'   : Outcome .
  vars QN QN'     : QualName .
  vars St St'     : Status .

  --- Equational simplification of Boolean expressions
  eq ~ c(true) = c(false) .
  eq ~ c(false) = c(true) .
  eq c(true) ^ iB = iB .
  eq c(false) ^ iB = c(false) .
  eq c(false) v iB = iB .
  eq c(true) v iB = c(true) .
  eq c(false) -> iB = c(true) .
  eq c(true) -> iB = iB .
  eq c(B) === c(B') = c(B == B') .
---(
  eq c(B) === c(B) = c(true) .
  eq c(true) === c(false) = c(false) .
  eq b(N) === b(N') = c(N == N') .
)
  eq iB =//= iB' = ~ (iB === iB') .

  --- Equational simplification of integer expressions
  eq c(I) === c(I') = c(I == I') .
---(
  eq c(s(I)) === c(s(I')) = c(I) === c(I') .
  eq c(0) === c(s(N)) = c(false) .
  eq i(N) === i(N') = c(N == N') .
)
  eq iI =//= iI' = ~ (iI === iI') .

  --- Evaluation of Boolean and integer expressions
  op eval : Configuration iExpr -> iExpr .

  -----------------------------
  ---- Boolean expressions ----
  -----------------------------

  --- Boolean constants
  eq eval(Cnf,c(B)) 
   = c(B) .

  --- Boolean variables
  eq eval(Cnf,b(N))
   = b(N) .

  --- Boolean local variable lookup
  eq eval( (< NeQN : mem | val |-> iB, AtS > Cnf), bmem(NeQN)) 
   = iB .

  --- Boolean external variable lookup
  eq eval( (< NeQN : extvar | type |-> boolean, values |-> (iB ; iEL), AtS > Cnf), blookup(NeQN)) 
   = iB .

  --- Negation
  eq eval(Cnf,~ iB) 
   = ~ eval(Cnf,iB) .

  --- Conjunction
  eq eval(Cnf,iB ^ iB')
   = (eval(Cnf,iB) ^ eval(Cnf,iB')) .

  --- Disjunction
  eq eval(Cnf,iB v iB')
   = eval(Cnf,iB) v eval(Cnf,iB') .

  --- Implication
  eq eval(Cnf,iB -> iB')
   = eval(Cnf,~ iB) v eval(Cnf,iB') .

  --- Equality
  eq eval(Cnf,iB === iB')
   = eval(Cnf, iB) === eval(Cnf,iB') .

  --- Inequality
  eq eval(Cnf,iB =//= iB')
   = eval(Cnf, iB) =//= eval(Cnf,iB') .

  -----------------------------
  ---- Integer expressions ----
  -----------------------------

  --- Integer constants
  eq eval(Cnf,c(I)) 
   = (c(I)) .

  --- Integer variables
  eq eval(Cnf,i(N))
   = i(N) .

  --- Integer local variable lookup
  eq eval( (< NeQN : mem | val |-> iI, AtS > Cnf), imem(NeQN)) 
   = iI .

  --- Integer external variable lookup
  eq eval( (< NeQN : extvar | type |-> int, values |-> (iI ; iEL), AtS > Cnf), ilookup(NeQN)) 
   = iI .

  --- Unary minus
  eq eval(Cnf,- iI)
   = - eval(Cnf,iI) .

  --- Addition
  eq eval(Cnf,iI + iI')
   = eval(Cnf,iI) + eval(Cnf,iI') .

  --- Product
  eq eval(Cnf,iI * iI')
   = eval(Cnf,iI) * eval(Cnf,iI') .

  --- Substraction
  eq eval(Cnf,iI - iI')
   = eval(Cnf,iI) - eval(Cnf,iI') .

  --------------------------------
  ---- Relational expressions ----
  --------------------------------

  --- At most
  eq eval(Cnf, iI <= iI')
   = eval(Cnf,iI) <= eval(Cnf,iI') .

  --- Less than
  eq eval(Cnf, iI < iI')
   = eval(Cnf,iI) < eval(Cnf,iI') .

  --- At least
  eq eval(Cnf, iI >= iI')
   = eval(Cnf,iI) >= eval(Cnf,iI') .

  --- Greater than
  eq eval(Cnf, iI > iI')
   = eval(Cnf,iI) > eval(Cnf,iI') .

  --- Equality
  eq eval(Cnf, iI === iI')
   = eval(Cnf,iI) === eval(Cnf,iI') .

  --- Innequality
  eq eval(Cnf, iI =//= iI')
   = eval(Cnf,iI) =//= eval(Cnf,iI') .

  --- Ancestor end true
  eq eval(Cnf,anc-end-true(NeQN))
   = eval(Cnf,collect-end-or(Cnf,NeQN) === c(true)) .

  --- Ancestor inv false
  eq eval(Cnf,anc-inv-false(NeQN))
   = eval(Cnf,collect-inv-and(Cnf,NeQN) === c(false)) .

  --- redefinition of highlevel prediates
  eq is-waiting(NeQN)
   = is-status(NeQN,waiting) .
  eq is-executing(NeQN)
   = is-status(NeQN,executing) .

  --- Is node in the given status?
  eq eval((< NeQN : Cls | status |-> St, AtS > Cnf),is-status(NeQN,St'))
   = if St == St'
     then c(true)
     else c(false)
     fi .

  --- Is node with the given outcome?
  eq eval((< NeQN : Cls | outcome |-> Out, AtS > Cnf),is-outcome(NeQN,Out'))
   = if Out == Out'
     then c(true)
     else c(false)
     fi .

  --- Is node with failure?
  eq eval((< NeQN : Cls | outcome |-> Out, AtS > Cnf),is-failure(NeQN))
   = if (Out == none) or-else (Out == skipped) or-else (Out == success)
     then c(false)
     else c(true)
     fi .

  --- Is parent failure?
  eq eval(Cnf,is-parent-failure(Out))
   = if Out == fail(parent)
     then c(true)
     else c(false)
     fi .

  --- Are all children waiting or finished?
  eq eval(Cnf,children-waiting-or-finished(NeQN))
   = waiting-or-finished(collect-children(Cnf,NeQN)) .

  --- Are all children finished?
  eq eval(Cnf,children-finished(NeQN))
   = finished(collect-children(Cnf,NeQN)) .

  -----------------------------
  ---- Auxiliary functions ----
  -----------------------------

  --- Collects the children of a given node
  op collect-children : Configuration NeQualName -> Configuration [memo] .
  op $collect-children : Configuration NeQualName Configuration -> Configuration .
  eq collect-children(Cnf,QN)
   = $collect-children(Cnf,QN,none) .
  eq $collect-children(none,QN,Cnf')
   = Cnf' .
  eq $collect-children((< Nm . QN' : Cls | AtS > Cnf), QN, Cnf')
   = if QN == QN'
     then $collect-children(Cnf, QN, (< Nm . QN : Cls | AtS > Cnf'))
     else $collect-children(Cnf, QN, Cnf')
     fi .

  --- Checks if the nodes in the collection are in state waiting or finished
  op waiting-or-finished : Configuration -> iBool .
  eq waiting-or-finished(none)
   = c(true) .
  eq waiting-or-finished(( < NeQN : mem | AtS > Cnf))
   = waiting-or-finished(Cnf) .
  eq waiting-or-finished(( < NeQN : Cls | status |-> St, AtS > Cnf ))
   = if St == waiting or St == finished
     then waiting-or-finished(Cnf)
     else c(false)
     fi .

  --- Checks if the nodes in the collection are in state waiting or finished
  op finished : Configuration -> iBool .
  eq finished(none)
   = c(true) .
  eq finished(( < NeQN : mem | AtS > Cnf))
   = finished(Cnf) .
  eq finished(( < NeQN : Cls | status |-> St, AtS > Cnf ))
   = if St == finished
     then finished(Cnf)
     else c(false)
     fi .

  --- Collects the end conditions of a herarchy using disjunction
  op collect-end-or : Configuration NeQualName -> iBool [memo] .
  op $collect-end-or : Configuration NeQualName iBool -> iBool .
  eq collect-end-or(Cnf,NeQN) 
   = $collect-end-or(Cnf,NeQN,c(false)) .
  eq $collect-end-or(Cnf,Nm,iB) 
   = iB .
  eq $collect-end-or((< NeQN : Cls | end |-> iB', AtS > Cnf), Nm . NeQN, iB)
   = $collect-end-or((< NeQN : Cls | end |-> iB', AtS > Cnf), NeQN, iB v iB') .

  --- Collects the invariant conditions of a herarchy using disjunction
  op collect-inv-and : Configuration NeQualName -> iBool [memo] .
  op $collect-inv-and : Configuration NeQualName iBool -> iBool .
  eq collect-inv-and(Cnf,NeQN) 
   = $collect-inv-and(Cnf,NeQN,c(true)) .
  eq $collect-inv-and(Cnf,Nm,iB) 
   = iB .
  eq $collect-inv-and((< NeQN : Cls | inv |-> iB', AtS > Cnf), Nm . NeQN, iB)
   = $collect-inv-and((< NeQN : Cls | inv |-> iB', AtS > Cnf), NeQN, iB ^ iB') .
endfm

--- Actions
fmod ACTION is
  pr QUALIFIED-NAME .
  pr STATUS .
  pr OUTCOME .
  pr EXPR .

  sort Action .
  op set-status  : NeQualName Status -> Action [ctor] .
  op set-outcome : NeQualName Outcome -> Action [ctor] .
  op set-value   : NeQualName iExpr -> Action [ctor] .
  op reset       : NeQualName -> Action [ctor] .
endfm

view Action from TRIV to ACTION is
  sort Elt to Action .
endv

--- Sets of actions
fmod ACTION-SET is
  pr SET{Action} * (sort NeSet{Action} to NeActionSet,
                    sort Set{Action} to ActionSet,
                    op empty to mtas,
                    op _,_ to _;_) .
endfm

view ActionSet from TRIV to ACTION-SET is
  sort Elt to ActionSet .
endv

--- Guarded action sets
fmod GUARDED-ACTION-SET is
  pr 2TUPLE{iBool,ActionSet} 
     * (sort Tuple{iBool,ActionSet} to 2GuardedActionSet) .

  pr 4TUPLE{String,Nat,iBool,ActionSet} 
     * (sort Tuple{String,Nat,iBool,ActionSet} to 4GuardedActionSet) .

  pr 5TUPLE{String,Nat,iBool,ActionSet,ActionSet} 
     * (sort Tuple{String,Nat,iBool,ActionSet,ActionSet} to 5GuardedActionSet) .

  pr 6TUPLE{String,Nat,iBool,ActionSet,iBool,ActionSet} 
     * (sort Tuple{String,Nat,iBool,ActionSet,iBool,ActionSet} to 6GuardedActionSet) .

  sort GuardedActionSet .
  subsorts 4GuardedActionSet 5GuardedActionSet 6GuardedActionSet < GuardedActionSet .
endfm

view 2GuardedActionSet from TRIV to GUARDED-ACTION-SET is
  sort Elt to 2GuardedActionSet .
endv

view 4GuardedActionSet from TRIV to GUARDED-ACTION-SET is
  sort Elt to 4GuardedActionSet .
endv

fmod ACTION-UTIL is
  pr GUARDED-ACTION-SET .
  pr PLX-CONFIG .
  pr EXT-BOOL .

  var  A          : Action .
  var  AS         : ActionSet .
  var  AtS        : AttributeSet .
  vars Cnf Cnf'   : Configuration .
  vars C C'       : Cid .
  vars iE iE'     : iExpr .
  var  Nm         : Name .
  vars NeQN NeQN' : NeQualName .
  var  Obj        : Object .
  var  Out Out'   : Outcome .
  var  QN         : QualName .
  vars St St'     : Status .

  --- returns the name of all target nodes affected
  --- by the given action in the given configuration
  op get-targets : Configuration Action -> OidSet .
  eq get-targets(Cnf,set-status(NeQN,St))
   = NeQN .
  eq get-targets(Cnf,set-outcome(NeQN,Out))
   = NeQN .
  eq get-targets(Cnf,set-value(NeQN,iE))
   = NeQN .
  eq get-targets(Cnf,reset(NeQN))
   = get-names(get-children-mem(Cnf,NeQN)) .

  --- applies the given action to the given configuration of nodes
  op apply-action   : Configuration Action -> Configuration .
  eq apply-action((Cnf < NeQN : C | status |-> St, AtS >),set-status(NeQN,St'))
   = Cnf < NeQN : C | status |-> St', AtS > .
  eq apply-action((Cnf < NeQN : C | outcome |-> Out, AtS >),set-outcome(NeQN,Out'))
   = Cnf < NeQN : C | outcome |-> Out', AtS > .
  eq apply-action((Cnf < NeQN : C | val |-> iE, AtS >),set-value(NeQN,iE'))
   = Cnf < NeQN : C | val |-> iE', AtS > .
  eq apply-action(Cnf,reset(NeQN))
   = reset-mems(Cnf,NeQN) .

  --- applies the given actions to the given configuration of nodes
  op apply-actions  : Configuration ActionSet -> Configuration .
  eq apply-actions(Cnf,(AS ; A))
   = apply-actions(apply-action(Cnf,A),AS) .
  eq apply-actions(Cnf,mtas)
   = Cnf .

  --- resets a given memory if the memory is a children of the
  --- given qualified name
  op reset-mem : Object NeQualName -> Object .
  eq reset-mem(( < Nm . NeQN : mem | val |-> iE, init-val |-> iE', AtS > ), NeQN)
   = < Nm . NeQN : mem | val |-> iE', init-val |-> iE', AtS > .
  eq reset-mem(Obj,NeQN)
   = Obj [owise] .

  op reset-mems  : Configuration NeQualName -> Configuration .
  op $reset-mems : Configuration NeQualName Configuration -> Configuration .
  eq reset-mems(Cnf,NeQN)
   = $reset-mems(Cnf,NeQN,(none).Configuration) .
  eq $reset-mems((none).Configuration,NeQN,Cnf')
   = Cnf' .
  eq $reset-mems((Cnf Obj),NeQN,Cnf')
   = $reset-mems(Cnf,NeQN,(Cnf' reset-mem(Obj,NeQN))) .
endfm

--- Sys of execution
fmod STATE is
  pr ACTION-UTIL .
  pr EVALUATION .

  sort Sys .
  subsort GuardedActionSet < Msg .
  --- abbreviated state; an abbreviated state is automatically rewriten
  --- into an equivalent full state; see equation below
  op {_} : Configuration -> Sys [prec 126 frozen] .
  --- full state
  op {_,_} : iBool Configuration -> Sys [prec 126 frozen(2)] .
  --- returns the guard of a state
  op guard : Sys -> iBool .
  --- returns the configuration in a state
  op conf : Sys -> Configuration .

  var  AtS        : AttributeSet .
  vars Cnf Cnf'   : Configuration .
  vars iB iB'     : iBool .
  vars iE iE'     : iExpr .
  vars iEL iEL'   : iExprList .
  var  NeQN       : NeQualName .

  --- abbreviated states are automatically rewriten into full states
  eq { Cnf } 
   = { collect-constraints(Cnf), Cnf } .

  --- projection of a full state into its first component
  eq guard(({ iB, Cnf })) 
   = iB .
  --- projection of a full state into its second component
  eq conf(({ iB, Cnf })) 
   = Cnf .

  --- Constant used to identify the current state of execution
  op #STATE# : -> Sys .

  op eval : Sys iExpr -> iExpr .
  --- Evaluation of expressions relative to the state dictated by constant #STATE#
  op eval : iExpr -> iExpr .
  eq eval(iE)
   = eval(#STATE#,iE) .
  eq eval(({ iB, Cnf }), iE) 
   = eval((Cnf),iE) .

  --- collects the constraints in the current timestep, if any
  op  collect-constraints : Configuration -> iBool .
  op $collect-constraints : Configuration iBool -> iBool .
  eq collect-constraints(Cnf)
   = $collect-constraints(Cnf,c(true)) .
  eq $collect-constraints(( < NeQN : extvar | constraints |-> iB ; iEL, AtS > Cnf), iB')
   = $collect-constraints(Cnf, iB' ^ iB) .
  eq $collect-constraints(Cnf,iB')
   = iB' [owise] .

  --- checks if there is an upcoming timestep, i.e.,
  --- if there is an external variable with at least two values and two constraints
  op has-future? : Configuration -> Bool .
  eq has-future?(( < NeQN : extvar | constraints |-> iB ; iEL, AtS > Cnf))
   = if iEL == nilel
     then false
     else true
     fi .
  eq has-future?(Cnf)
   = false [owise] .

  --- updates the external variables with the next available value and constraint.
  --- If an external variable does not have a second available value and constraint,
  --- then it keeps the current value and constraint
  op  pop-env : Configuration -> Configuration .
  op $pop-env : Configuration Configuration -> Configuration .
  eq pop-env(Cnf)
   = $pop-env(Cnf,none) .
  eq $pop-env(( < NeQN : extvar | values |-> iE ; iEL, constraints |-> iE' ; iEL', AtS > Cnf), Cnf')
   = if iEL == nilel
     then $pop-env(Cnf, ( < NeQN : extvar | values |-> iE, constraints |-> iE', AtS > Cnf'))
     else $pop-env(Cnf, ( < NeQN : extvar | values |-> iEL, constraints |-> iEL', AtS > Cnf'))
     fi .
  eq $pop-env(Cnf,Cnf')
   = Cnf Cnf' [owise] .

  sort SysSet .
  subsort Sys < SysSet .
  op mtstset : -> SysSet [ctor] .
  op __ : SysSet SysSet -> SysSet [assoc comm id: mtstset] .
endfm