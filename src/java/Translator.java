import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.nianet.plexil.plexilxml2maude.PlexilPlanLoadException;
import org.nianet.plexil.plexilxml2maude.PlexilXMLUnmarshaller;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ADD;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.AND;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.DeclareVariable;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.EQBoolean;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.EQNumeric;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.GE;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.GT;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.LE;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.LT;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.LookupNow;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.LookupOnChange;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.MUL;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.NEBoolean;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.NENumeric;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.NOT;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.Node;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.NodeOutcomeVariable;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.NodeStateVariable;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.OR;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.PlexilPlan;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.SUB;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.ArithmeticExpression;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.BooleanExpression;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.BooleanLiteral;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.BooleanVariable;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.NodeVariable;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.NumericalLiteral;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.PlexilNodeCondition;
import org.nianet.plexil.plexilxml2maude.jaxbmodel.ext.StateOutcomeCheckingExpression;
import org.nianet.plexil.scriptcontext.jaxbmodel.generated.PLEXILScript;
import org.nianet.plexil.scriptcontext.jaxbmodel.generated.RandomValue;

import plexil.Ple2XmlConverter;

public class Translator {

	public static final String BOOL = "boolean";
	public static final String INT = "int";
	public static final String ASSG = "assg";
	public static final String INIT = "init";
	public static final String LIST = "list";
	public static final String MEM = "mem";
	public static final String NAME = "Name";
	public static final String NODE = "node";
	public static final String OBJ = "Object";
	public static final String STATE = "Sys";
	public static final String SYNTAX = "SYNTAX";
	public static final String VAR = "mem";
	public static final String EXTVAR = "extvar";
	public static final String LOOKUP = "lookup";

	// relational operators
	public static final String S_EQU = " === ";
	public static final String S_NEQU = " =//= ";
	public static final String S_GET = " >= ";
	public static final String S_GT = " > ";
	public static final String S_LET = " <= ";
	public static final String S_LT = " < ";

	// arithmetic operators
	public static final String S_ADD = " + ";
	public static final String S_MUL = " * ";
	public static final String S_SUB = " - ";

	// Boolean operators
	public static final String S_NOT = " ~ ";
	public static final String S_AND = " ^ ";
	public static final String S_OR = " v ";

	// local variables
	public static final String S_BLVAR = "bmem";
	public static final String S_ILVAR = "imem";

	// external variables
	public static final String S_BEVAR = "blookup";
	public static final String S_IEVAR = "ilookup";

	// constants
	public static final String S_CONS = "c";

	// UNIX terminal escape sequences
	private static final String normal = "\033[0m";
	private static final String bold = "\033[1m";
	private static final String underline = "\033[4m";
	private static final String yellow = "\033[33m";
	private static final String white = "\033[37m";
	private static final String boldyellow = "\033[33;1m";
	private static final String boldwhite = "\033[37;1m";

	private static PlexilPlan plan = null;
	private static PLEXILScript script = null;
	private static ArrayList<String> nodeNames = null;
	private static ArrayList<String> varNames = null;
	private static ArrayList<String> extvarNames = null;
	private static ArrayList<String> allNames = null;

	private static Map<String, String> nameMap = null;
	private static Map<String, String> extnameMap = null;
	private static Hashtable<Node, String> nameHash = null;
	private static Hashtable<DeclareVariable, String> varHash = null;
	private static Hashtable<Node, Node> parentHash = null;

	private static StringBuffer sb = null;

	// global counters for external integer and Boolean values
	private static int ivars = 0;
	private static int bvars = 0;

	// command line parameters
	private static boolean checkInv = false;
	private static boolean checkPre = false;
	private static boolean checkPost = false;
	private static boolean checkRace = false;
	private static boolean verbose = false;
	private static boolean interactive = false;
	private static boolean checkForm = false;
	private static String formula = null;

	public static void main(String[] args) throws Exception {
		int i, lasti;
		i = lasti = 0;
		String plexilFile = null;
		String scriptFile = null;
		String schemaFile = null;

		try {
			while (i < args.length) {
				if (args[i].equals("-h") || args[i].equals("--help")) {
					helpMessage();
					System.exit(1);
				} else if (args[i].equals("-p") || args[i].equals("--plan")) {
					plexilFile = args[++i];
				} else if (args[i].equals("-s") || args[i].equals("--script")) {
					scriptFile = args[++i];
				} else if (args[i].equals("-c") || args[i].equals("--schema")) {
					schemaFile = args[++i];
				} else if (args[i].equals("-f") || args[i].equals("--formula")) {
					checkForm = true;
					formula = args[++i];
				} else if (args[i].equals("-i")
						|| args[i].equals("--check-inv")) {
					checkInv = true;
				} else if (args[i].equals("-e")
						|| args[i].equals("--check-pre")) {
					checkPre = true;
				} else if (args[i].equals("-o")
						|| args[i].equals("--check-post")) {
					checkPost = true;
				} else if (args[i].equals("-u")
						|| args[i].equals("--user-interaction")) {
					interactive = true;
				} else if (args[i].equals("-r")
						|| args[i].equals("--check-race")) {
					checkRace = true;
				} else if (args[i].equals("-v") || args[i].equals("--verbose")) {
					verbose = true;
				} else if (i == lasti) {// when not all of the options have been
										// cleared, ERROR
					throw new Exception("Illegal command line argument: "
							+ args[i]);
				} else {
					System.exit(1);
				}
				i++;
				lasti = i;
			}

		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
			helpMessage();
			System.exit(1);
		}

		/*
		 * old style: fixed order arguments String pleFile = args[0]; String
		 * scriptFile = args[1]; String schemaFile = args[2];
		 */
		if (plexilFile == null || scriptFile == null || schemaFile == null) {
			helpMessage();
			System.exit(1);
		} else {
			String plx = renameFileExtension(plexilFile, ".plx");
			String plxc = renameFileExtension(plexilFile, ".plxc");
			String maude = renameFileExtension(plexilFile, ".sra.maude");

			System.out.println("Translation process started ...");
			loadScript(scriptFile);
			ple2plx(plexilFile, plx);
			plx2plxc(plx, plxc, schemaFile);
			plxc2sram(plxc, maude);
			System.out.println("Done.");
		}
	}

	public static void helpMessage() {
		String out = bold + yellow + "NAME\n\t" + normal
				+ "ple2sram - generates maude specs of plexil plans.\n\n";
		out += bold + yellow + "SYNOPSIS\n";
		out += "\tple2sram " + boldwhite + "[" + yellow + "-h" + boldwhite
				+ "] ";
		out += "[" + yellow + "-p " + boldwhite + underline + "plan" + normal
				+ boldwhite + "] ";
		out += "[" + yellow + "-s " + boldwhite + underline + "script" + normal
				+ boldwhite + "] ";
		out += "[" + yellow + "-c " + boldwhite + underline + "schema" + normal
				+ boldwhite + "] ";
		out += "[" + yellow + "-u" + boldwhite + "] ";
		out += "[" + yellow + "-v" + boldwhite + "] ";
		out += "[" + yellow + "-f " + boldwhite + underline + "formula"
				+ normal + boldwhite + "] ";
		out += "[" + yellow + "-i" + boldwhite + "] ";
		out += "[" + yellow + "-e" + boldwhite + "] ";
		out += "[" + yellow + "-o" + boldwhite + "] ";
		out += "[" + yellow + "-r" + boldwhite + "] \n\n";
		out += bold + yellow + "OPTIONS\n";
		out += "\t" + yellow + "-h, --help\n";
		out += "\t\t" + normal + boldwhite
				+ "Help screen with all options.\n\n";
		out += "\t" + yellow + "-p, --plan " + boldwhite + underline
				+ "filename" + normal + "\n";
		out += "\t\t" + normal + boldwhite + "Plexil plan file.\n\n";
		out += "\t" + yellow + "-s, --script " + boldwhite + underline
				+ "filename" + normal + "\n";
		out += "\t\t" + normal + boldwhite + "Plexil script file.\n\n";
		out += "\t" + yellow + "-c, --schema " + boldwhite + underline
				+ "filename" + normal + "\n";
		out += "\t\t" + normal + boldwhite + "Schema file.\n\n";
		out += "\t" + yellow + "-u, --user-interaction\n";
		out += "\t\t" + normal + boldwhite
				+ "Allow user interaction in Maude.\n\n";
		out += "\t" + yellow + "-v, --verbose\n";
		out += "\t\t" + normal + boldwhite + "Produce counter-examples.\n\n";
		out += "\t" + yellow + "-f, --check-formula " + boldwhite + underline
				+ "formula" + normal + "\n";
		out += "\t\t" + normal + boldwhite + "LTL formula to verify.\n\n";
		out += "\t" + yellow + "-i, --check-inv\n";
		out += "\t\t" + normal + boldwhite + "Check invariant conditions.\n\n";
		out += "\t" + yellow + "-e, --check-pre\n";
		out += "\t\t" + normal + boldwhite + "Check pre-conditions.\n\n";
		out += "\t" + yellow + "-o, --check-post\n";
		out += "\t\t" + normal + boldwhite + "Check post-conditions.\n\n";
		out += "\t" + yellow + "-r, --check-race\n";
		out += "\t\t" + normal + boldwhite + "Check race conditions.\n\n";
		out += normal;
		System.out.println(out);
	}

	public static String renameFileExtension(String source, String newExtension) {
		String currentExtension = "";
		String target;
		int pos = source.lastIndexOf('.');
		if (pos > 0 && pos < source.length() - 1) {
			currentExtension = source.substring(pos + 1);
		}

		if (currentExtension.equals("")) {
			target = source + "." + newExtension;
		} else {
			target = source.substring(0, pos) + newExtension;
		}
		return target;
	}

	public static void loadScript(String scriptname) {
		File scriptfile = new File(scriptname);
		if (scriptfile.exists()) {
			try {
				script = PlexilXMLUnmarshaller.getInstance()
						.getPlexilScriptFromXML(scriptname);
			} catch (PlexilPlanLoadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/* Piece-wise translations */

	public static void ple2plx(String source, String target) throws Exception {
		// load the source file
		System.out.println("\t translating PLE file " + source
				+ " to PLX format " + target);

		String text = Ple2XmlConverter.getXMLFromPLE(source);

		BufferedWriter bw = new BufferedWriter(new FileWriter(target));
		bw.write(text);
		bw.close();
	}

	public static void plx2plxc(String source, String target, String schema)
			throws Exception {
		// saxon arguments
		String[] saxonArgs = new String[4];
		saxonArgs[0] = "-o";
		saxonArgs[1] = target;
		saxonArgs[2] = source;
		saxonArgs[3] = schema.replaceAll("%20", " ");

		System.out.println("\t translating PLX file " + source
				+ " to PLXC format " + target);

		// perform the translation using the schema
		net.sf.saxon.Transform.main(saxonArgs);

		// translate
		// 1. end of line to \r\n
		// 2. underscores to dashes so that maude does not complain
		BufferedReader reader = new BufferedReader(new FileReader(target));
		StringBuffer strbf = new StringBuffer();

		// change end of lines
		while (reader.ready()) {
			strbf.append(reader.readLine() + "\r\n");
		}
		reader.close();

		// change underscores by dashes
		FileWriter writer = new FileWriter(target);
		writer.write(strbf.toString().replaceAll("_", "-"));
		writer.close();
	}

	public static void plxc2maude(String source, String target)
			throws Exception {
		// System.err.println("Translating PLXC file " + source + " to Maude "
		// + target + "...");

		JAXBContext context = JAXBContext
				.newInstance("org.nianet.plexil.plexilxml2maude.jaxbmodel");
		Unmarshaller unmarshaller = context.createUnmarshaller();

		InputStream is = new FileInputStream(source);
		plan = (PlexilPlan) unmarshaller.unmarshal(is);

		// update nodes with variables and absolute names
		plan.fillOnNodesAbsolutePath();
		plan.setNodesLexicalScopedVariables();
		// Node root = plan.getRootNode();
		String maudeModule = plan.generateMaudeModule(null);

		BufferedWriter bw = new BufferedWriter(new FileWriter(target));
		bw.write(maudeModule);
		bw.close();

		// System.err.println("Done.");
	}

	/* Collect variables info */

	public static void collectAbsoluteNames(Node node, String prefix) {
		String qualName = prefix.length() == 0 ? node.getNodeId() : node
				.getNodeId() + " . " + prefix;
		nameHash.put(node, qualName);
		if (node.getChildNodes() != null) {
			for (Node child : node.getChildNodes()) {
				collectAbsoluteNames(child, qualName);
			}
		}
		if (node.getVariables() != null) {
			for (DeclareVariable var : node.getVariables()) {
				varHash.put(var, var.getName() + " . " + qualName);
			}
		}

	}

	public static void collectParents(Node node, Node parent) {
		parentHash.put(node, parent);
		if (node.getChildNodes() != null) {
			for (Node child : node.getChildNodes()) {
				collectParents(child, node);
			}
		}
	}

	public static String collectVarNameInScope(Node node, String varname) {
		String r = null;
		if (node.getVariables() != null) {
			boolean b = false;
			for (int i = 0; !b && i < node.getVariables().size(); i++) {
				DeclareVariable var = node.getVariables().get(i);
				if (var.getName().equals(varname)) {
					r = varHash.get(var);
					b = true;
				}
			}
			if (!b) {
				if (!parentHash.get(node).equals(node)) {
					r = collectVarNameInScope(parentHash.get(node), varname);
				} else {
					throw new RuntimeException("no variable '" + varname
							+ "' in the scope!");
				}
			}
		} else {
			if (!parentHash.get(node).equals(node)) {
				r = collectVarNameInScope(parentHash.get(node), varname);
			} else {
				throw new RuntimeException("no variable '" + varname
						+ "' in the scope!");
			}
		}
		return r;
	}

	public static void getQualifiedNodeNames(String prefix, Node node,
			ArrayList<String> list) {
		if (!prefix.equals("")) {
			prefix = node.getNodeId() + " . " + prefix;
		} else {
			prefix = node.getNodeId();
		}
		list.add(prefix);

		if (node.getChildNodes() != null) {
			List<Node> childNodes = node.getChildNodes();
			for (Node child : childNodes) {
				getQualifiedNodeNames(prefix, child, list);
			}
		}
	}

	public static void getNodeNames(Node node, ArrayList<String> list) {
		list.add(node.getNodeId());
		if (node.getChildNodes() != null) {
			List<Node> childNodes = node.getChildNodes();
			for (Node child : childNodes) {
				getNodeNames(child, list);
			}
		}
	}

	public static void getQualifiedVariableNames(String prefix, Node node,
			ArrayList<String> list) {
		if (!prefix.equals("")) {
			prefix = node.getNodeId() + " . " + prefix;
		} else {
			prefix = node.getNodeId();
		}
		if (node.getVariables() != null) {
			for (DeclareVariable var : node.getVariables()) {
				list.add(var.getName() + " . " + prefix);
			}
		}
		if (node.getChildNodes() != null) {
			List<Node> childNodes = node.getChildNodes();
			for (Node child : childNodes) {
				getQualifiedVariableNames(prefix, child, list);
			}
		}
	}

	public static void getVariableNames(Node node, ArrayList<String> list) {
		if (node.getVariables() != null) {
			for (DeclareVariable var : node.getVariables()) {
				list.add(var.getName());
			}
		}
		if (node.getChildNodes() != null) {
			List<Node> childNodes = node.getChildNodes();
			for (Node child : childNodes) {
				getVariableNames(child, list);
			}
		}
	}

	public static void getExternalVariables() {
		extnameMap = new Hashtable<String, String>();
		extvarNames = new ArrayList<String>();
		if (script != null) {
			for (PLEXILScript.Step step : script.getStep()) {
				for (PLEXILScript.Step.State state : step.getState()) {
					String shortname = new String(state.getName());
					/* if fully qualified var names are used, uncomment */
					/*
					 * String name = new String(state.getName()); String
					 * shortname = new String(name); int pos =
					 * name.indexOf('.'); if (pos > 0 && pos < name.length() -
					 * 1) { shortname = name.substring(pos + 1); }
					 */
					if (!extnameMap.containsKey(shortname)) {
						String type = new String(state.getType());
						extnameMap.put(shortname, type);
						extvarNames.add(shortname);
						// System.out.println("External var: "+shortname+" : "+type);
					}
				}
			}
		}
	}

	public static String getExtVarType(String v) {
		String t = extnameMap.get(v);
		if (v == null)
			return "Unknown";
		else
			return new String(t);
	}

	public static void initNameMapping(ArrayList<String> names) {
		nameMap = new Hashtable<String, String>();
		for (String val : names) {
			String key = new String(val);
			nameMap.put(key.split("\\.")[0].trim(), val);
		}
	}

	/* declarations */

	public static void declareNodes() {
		// node names
		sb.append("  --- node names and declarations\n");
		sb.append("  ops ");
		for (String nodeName : nodeNames) {
			sb.append(nodeName.split("\\.")[0].trim() + " ");
		}
		sb.append(": -> " + NAME + " [ctor] .\n");
		// node declarations
		sb.append("  ops ");
		for (String nodeName : nodeNames) {
			sb.append(nodeName.split("\\.")[0].trim() + "-" + NODE + " ");
		}
		sb.append(": -> " + OBJ + " .\n");
	}

	public static void declareVariables() {
		if (varNames.size() > 0) {
			// variable names
			sb.append("  --- local variable names and declarations\n");
			sb.append("  ops ");
			for (String varName : varNames) {
				sb.append(varName.split("\\.")[0].trim() + " ");
			}
			sb.append(": -> " + NAME + " [ctor] .\n");
			// variable declarations
			sb.append("  ops ");
			for (String varName : varNames) {
				sb.append(varName.split("\\.")[0].trim() + "-" + VAR + " ");
			}
			sb.append(": -> " + OBJ + " .\n");
		}

		if (extvarNames.size() > 0) {
			// lookup variable names
			sb.append("  --- external variable names and declarations\n");
			sb.append("  ops ");
			for (String varName : extvarNames) {
				sb.append(varName.split("\\.")[0].trim() + " ");
			}
			sb.append(": -> " + NAME + " [ctor] .\n");
			// lookup variable declarations
			sb.append("  ops ");
			for (String varName : extvarNames) {
				sb.append(varName.split("\\.")[0].trim() + "-" + LOOKUP + " ");
			}
			sb.append(": -> " + OBJ + " .\n");
		}

	}

	public static void declareFunctionalModule() {
		sb.append("fmod " + plan.getRootNode().getNodeId().toUpperCase() + "-"
				+ SYNTAX + " is\n  protecting STATE .\n");
		declareNodes();
		declareVariables();
		processNode(plan.getRootNode(), "");
		processExternalVars();
		sb.append("\n\nendfm");
	}

	public static void declareNonfunctionalModule() {
		sb.append("\n\nmod " + plan.getRootNode().getNodeId().toUpperCase()
				+ " is\n  protecting "
				+ plan.getRootNode().getNodeId().toUpperCase() + "-" + SYNTAX
				+ " .\n  protecting SRA-PLX .\n");
		sb.append("\n  --- qualified name of this module\n");
		sb.append("  eq USER-MODULE-NAME = '"
				+ plan.getRootNode().getNodeId().toUpperCase() + " .\n");
		sb.append("\n  --- initial state\n");
		sb.append("  op " + INIT + " : -> " + STATE + " .\n");
		sb.append("  eq init = { ");
		for (String nodeName : nodeNames) {
			sb.append(nodeName.split("\\.")[0].trim() + "-" + NODE + " ");
		}
		for (String varName : varNames) {
			sb.append(varName.split("\\.")[0].trim() + "-" + VAR + " ");
		}
		for (String evarName : extvarNames) {
			sb.append(evarName + "-" + LOOKUP + " ");
		}
		sb.append("} .\n");
		sb.append("endm");
	}

	public static String processListNode(Node node, String prefix) {
		String r = new String();
		boolean isRoot = prefix.equals("");
		String qualName = node.getNodeId() + (isRoot ? "" : " . " + prefix);
		PlexilNodeCondition stac = (PlexilNodeCondition) node
				.getStartCondition();
		PlexilNodeCondition skic = (PlexilNodeCondition) node
				.getSkipCondition();
		PlexilNodeCondition endc = (PlexilNodeCondition) node.getEndCondition();
		PlexilNodeCondition repc = (PlexilNodeCondition) node
				.getRepeatCondition();
		PlexilNodeCondition prec = (PlexilNodeCondition) node.getPreCondition();
		PlexilNodeCondition posc = (PlexilNodeCondition) node
				.getPostCondition();
		PlexilNodeCondition invc = (PlexilNodeCondition) node
				.getInvariantCondition();
		r += "  eq " + node.getNodeId() + "-" + NODE + "\n   = < " + qualName
				+ " : " + LIST + " |\n";
		r += "\tparent |-> " + (isRoot ? "nil" : prefix) + ", ";
		r += "status |-> inactive, ";
		r += "outcome |-> none,\n";
		r += "\tstart |-> "
				+ (stac == null ? "c(true)" : boolExprTrans(node,
						stac.getCondition())) + ", ";
		r += "skip |-> "
				+ (skic == null ? "c(false)" : boolExprTrans(node,
						skic.getCondition())) + ", ";
		r += "end |-> "
				+ (endc == null ? "children-finished(" + qualName + ")"
						: boolExprTrans(node, endc.getCondition())) + ",\n";
		r += "\trepeat |-> "
				+ (repc == null ? "c(false)" : boolExprTrans(node,
						repc.getCondition())) + ", ";
		r += "pre |-> "
				+ (prec == null ? "c(true)" : boolExprTrans(node,
						prec.getCondition())) + ", ";
		r += "post |-> "
				+ (posc == null ? "c(true)" : boolExprTrans(node,
						posc.getCondition())) + ", ";
		r += "inv |-> "
				+ (invc == null ? "c(true)" : boolExprTrans(node,
						invc.getCondition()));
		r += " > .\n";
		return r;
	}

	public static String processAssignmentNode(Node node, String prefix) {
		String r = new String();
		boolean isRoot = prefix.equals("");
		String qualName = node.getNodeId() + (isRoot ? "" : " . " + prefix);
		PlexilNodeCondition stac = (PlexilNodeCondition) node
				.getStartCondition();
		PlexilNodeCondition skic = (PlexilNodeCondition) node
				.getSkipCondition();
		PlexilNodeCondition endc = (PlexilNodeCondition) node.getEndCondition();
		PlexilNodeCondition repc = (PlexilNodeCondition) node
				.getRepeatCondition();
		PlexilNodeCondition prec = (PlexilNodeCondition) node.getPreCondition();
		PlexilNodeCondition posc = (PlexilNodeCondition) node
				.getPostCondition();
		PlexilNodeCondition invc = (PlexilNodeCondition) node
				.getInvariantCondition();
		r += "  eq " + node.getNodeId() + "-" + NODE + "\n   = < " + qualName
				+ " : " + ASSG + " |\n";
		r += "\tparent |-> " + (isRoot ? "nil" : prefix) + ", ";
		r += "status |-> inactive, ";
		r += "outcome |-> none,\n";
		r += "\tstart |-> "
				+ (stac == null ? "c(true)" : boolExprTrans(node,
						stac.getCondition())) + ", ";
		r += "skip |-> "
				+ (skic == null ? "c(false)" : boolExprTrans(node,
						skic.getCondition())) + ", ";
		r += "end |-> "
				+ (endc == null ? "c(true)" : boolExprTrans(node,
						endc.getCondition())) + ",\n";
		r += "\trepeat |-> "
				+ (repc == null ? "c(false)" : boolExprTrans(node,
						repc.getCondition())) + ", ";
		r += "pre |-> "
				+ (prec == null ? "c(true)" : boolExprTrans(node,
						prec.getCondition())) + ", ";
		r += "post |-> "
				+ (posc == null ? "c(true)" : boolExprTrans(node,
						posc.getCondition())) + ", ";
		r += "inv |-> "
				+ (invc == null ? "c(true)" : boolExprTrans(node,
						invc.getCondition())) + ", \n";

		String varName = null;
		String expr = null;
		if (node.getNodeBody().getAssignment().getIntegerVariable() != null) {
			varName = node.getNodeBody().getAssignment().getIntegerVariable();
			expr = intExprTrans(node, node.getNodeBody().getAssignment()
					.getNumericRHS().getArithmeticExpression());
		} else if (node.getNodeBody().getAssignment().getBooleanVariable() != null) {
			varName = node.getNodeBody().getAssignment().getBooleanVariable();
			expr = boolExprTrans(node, node.getNodeBody().getAssignment()
					.getBooleanRHS().getBooleanExpression());
		} else {
			throw new RuntimeException("Expression type not supported: "
					+ node.getNodeBody().getAssignment().toString());
		}
		r += "\tset(" + collectVarNameInScope(node, varName) + ") |-> " + expr;

		// // System.out.println("---------- " + varName);
		// NumericRHS nrhs = node.getNodeBody().getAssignment().getNumericRHS();
		// if (nrhs != null) {
		// String nexpr = nrhs.getArithmeticExpression()
		// .getExpression(nameMap);
		// r += "\tset(" + nameMap.get(varName) + ") |-> " + toSram(nexpr);
		// } else {
		// BooleanRHS brhs = node.getNodeBody().getAssignment()
		// .getBooleanRHS();
		// String bexpr = brhs.getBooleanExpression().getExpression(nameMap);
		// r += "\tset(" + nameMap.get(varName) + ") |-> " + toSram(bexpr);
		// }
		r += " > .\n";
		return r;
	}

	public static String processVariable(DeclareVariable var, String prefix) {
		String r = new String();
		String qualName = var.getName() + " . " + prefix;
		r += "  eq " + var.getName() + "-" + VAR + "\n   = < " + qualName
				+ " : " + MEM + " |\n";
		r += "\tparent |-> " + prefix + ", ";
		String initval = toSram(var.getInitialValueExpression(nameMap));
		r += "val |-> " + initval + ", ";
		r += "init-val |-> " + initval;
		r += " > .\n";
		return r;
	}

	public static void processNode(Node node, String prefix) {
		String nodeType = node.getNodeType();

		if (nodeType.equals("NodeList")) {
			sb.append("\n" + processListNode(node, prefix));
			prefix = node.getNodeId()
					+ (prefix.length() == 0 ? "" : " . " + prefix);
			for (DeclareVariable var : node.getVariables()) {
				sb.append("\n" + processVariable(var, prefix));
			}
			for (Node child : node.getChildNodes()) {
				processNode(child, prefix);
			}
		} else if (nodeType.equals("Assignment")) {
			sb.append("\n" + processAssignmentNode(node, prefix));
		} else {
			throw new RuntimeException("Invalid type of node: " + nodeType);
		}
	}

	public static String boolExprTrans(Node node, BooleanExpression expr) {
		String r = "";
		if (expr instanceof EQBoolean) {
			r = processBooleanArguments(node,
					((EQBoolean) expr).getBooleanExpression(), S_EQU, "");
		} else if (expr instanceof NEBoolean) {
			r = processBooleanArguments(node,
					((EQBoolean) expr).getBooleanExpression(), S_NEQU, "");
		} else if (expr instanceof EQNumeric) {
			r = processIntArguments(node,
					((EQNumeric) expr).getNumericExpression(), S_EQU, "");
		} else if (expr instanceof NENumeric) {
			r = processIntArguments(node,
					((NENumeric) expr).getNumericExpression(), S_NEQU, "");
		} else if (expr instanceof GE) {
			r = processIntArguments(node, ((GE) expr).getNumericExpression(),
					S_GET, "");
		} else if (expr instanceof LE) {
			r = processIntArguments(node, ((LE) expr).getNumericExpression(),
					S_LET, "");
		} else if (expr instanceof GT) {
			r = processIntArguments(node, ((GT) expr).getNumericExpression(),
					S_GT, "");
		} else if (expr instanceof LT) {
			r = processIntArguments(node, ((LT) expr).getNumericExpression(),
					S_LT, "");
		} else if (expr instanceof AND) {
			r = processBooleanArguments(node,
					((AND) expr).getBooleanExpression(), S_AND, "");
		} else if (expr instanceof OR) {
			r = processBooleanArguments(node,
					((OR) expr).getBooleanExpression(), S_OR, "");
		} else if (expr instanceof NOT) {
			r = S_NOT + boolExprTrans(node, ((NOT) expr).getCondition());
		} else if (expr instanceof LookupOnChange) {
			r = S_BEVAR + "("
					+ ((LookupOnChange) expr).getName().getStringValue() + ")";
		} else if (expr instanceof LookupNow) {
			r = S_BEVAR + "(" + ((LookupNow) expr).getName().getStringValue()
					+ ")";
		} else if (expr instanceof BooleanLiteral) {
			r = S_CONS + "(" + ((BooleanLiteral) expr).getValue() + ")";
		} else if (expr instanceof BooleanVariable) {
			r = S_BLVAR
					+ "("
					+ collectVarNameInScope(node,
							((BooleanVariable) expr).getVarname()) + ")";
		} else if (expr instanceof StateOutcomeCheckingExpression) {
			r = processStateOutcomeCheckingExpression(node,
					(StateOutcomeCheckingExpression) expr);
		} else {
			throw new RuntimeException("Unsupported Boolean operation: " + expr);
		}
		return "(" + r + ")";
	}

	public static String processBooleanArguments(Node node, List<Object> args,
			String oper, String id) {
		String r = null;
		if (args.size() == 0) {
			r = id;
		} else {
			ArrayList<String> sargs = new ArrayList<String>();

			for (Object o : args) {
				if (o instanceof BooleanExpression) {
					sargs.add(boolExprTrans(node, (BooleanExpression) o));
				} else if (o instanceof JAXBElement) {
					JAXBElement temp = (JAXBElement) o;
					if (temp.getDeclaredType() == Boolean.class) {
						sargs.add(S_CONS + "(" + temp.getValue() + ")");
					} else if (temp.getDeclaredType() == String.class) {
						sargs.add(S_BLVAR
								+ "("
								+ collectVarNameInScope(node, temp.getValue()
										.toString()) + ")");
					} else {
						throw new RuntimeException(
								"unsupported Boolean literal: "
										+ temp.getDeclaredType());
					}
				} else {
					sargs.add("Unsupported Boolean: " + o.getClass());
				}
			}
			r = sargs.get(0);
			for (int i = 1; i < sargs.size(); i++) {
				r += oper + sargs.get(i);
			}
		}
		return "(" + r + ")";
	}

	public static String processStateOutcomeCheckingExpression(Node node,
			StateOutcomeCheckingExpression expr) {
		String r = null;
		if (expr.isStateCheckingExpression()) {
			r = "is-status("
					+ getNodeName(expr.getNodeState().get(0), expr
							.getNodeState().get(1)) + ","
					+ processStatus(expr.getVariableValue()) + ")";
		} else if (expr.isOutcomeCheckingExpression()) {
			r = "is-outcome("
					+ getNodeName(expr.getNodeOutcome().get(0), expr
							.getNodeOutcome().get(1)) + ","
					+ processOutcome(expr.getVariableValue()) + ")";
		}
		return r;
	}

	public static String getNodeName(Object o1, Object o2) {
		String r = null;
		if (o1 instanceof NodeStateVariable) {
			r = nameMap.get(((NodeStateVariable) o1).getNodeId());
		} else if (o2 instanceof NodeStateVariable) {
			r = nameMap.get(((NodeStateVariable) o2).getNodeId());
		} else if (o1 instanceof NodeOutcomeVariable) {
			r = nameMap.get(((NodeOutcomeVariable) o1).getNodeId());
		} else if (o2 instanceof NodeOutcomeVariable) {
			r = nameMap.get(((NodeOutcomeVariable) o2).getNodeId());
		} else {
			throw new RuntimeException(
					"Value no supported in staus and outcome checks: " + o1
							+ " : " + o2);
		}

		return r;
	}

	public static String processStatus(String status) {
		return status;
	}

	public static String processOutcome(String outcome) {
		return outcome;
	}

	public static String intExprTrans(Node node, ArithmeticExpression expr) {
		String r = "";
		if (expr instanceof ADD) {
			r = processIntArguments(node, ((ADD) expr).getNumericExpression(),
					S_ADD, "0");
		} else if (expr instanceof SUB) {
			r = processIntArguments(node, ((SUB) expr).getNumericExpression(),
					S_SUB, "0");
		} else if (expr instanceof MUL) {
			r = processIntArguments(node, ((MUL) expr).getNumericExpression(),
					S_MUL, "1");
		} else if (expr instanceof LookupOnChange) {
			r = S_IEVAR + "("
					+ ((LookupOnChange) expr).getName().getStringValue() + ")";
		} else if (expr instanceof NodeVariable) {
			r = S_ILVAR
					+ "("
					+ collectVarNameInScope(node,
							((NodeVariable) expr).getValue()) + ")";
		} else if (expr instanceof NumericalLiteral) {
			r = S_CONS + "(" + ((NumericalLiteral) expr).getValue() + ")";
		} else {
			throw new RuntimeException("Unsupported arithmetic operation: "
					+ expr);
		}

		return "(" + r + ")";
	}

	public static String processIntArguments(Node node, List<Object> args,
			String oper, String id) {
		String r = null;
		if (args.size() == 0) {
			r = id;
		} else {
			ArrayList<String> sargs = new ArrayList<String>();

			for (Object o : args) {
				if (o instanceof ArithmeticExpression) {
					sargs.add(intExprTrans(node, (ArithmeticExpression) o));
				} else if (o instanceof JAXBElement) {
					JAXBElement temp = (JAXBElement) o;
					if (temp.getDeclaredType() == BigInteger.class) {
						sargs.add(S_CONS + "(" + temp.getValue() + ")");
					} else if (temp.getDeclaredType() == String.class) {
						sargs.add(S_ILVAR
								+ "("
								+ collectVarNameInScope(node, temp.getValue()
										.toString()) + ")");
					} else {
						throw new RuntimeException(
								"unsupported arithmetic literal: "
										+ temp.getDeclaredType());
					}
				} else if (o instanceof LookupOnChange) {
					sargs.add(S_IEVAR + "("
							+ ((LookupOnChange) o).getName().getStringValue()
							+ ")");
				} else {
					sargs.add("Unsupported arithmetic: " + o.getClass());
				}
			}
			r = sargs.get(0);
			for (int i = 1; i < sargs.size(); i++) {
				r += oper + sargs.get(i);
			}
		}
		return r;
	}

	public static String translateLookup(String expr) {
		/* replace LookupNow/LookupOnChange(v,t) with ilookup/blookup(v) */
		/* de-paranthesize by force, parantheses seem to throw the matcher off */
		int len = expr.length();
		while (expr.startsWith("(") && expr.endsWith(")")) {
			expr = expr.substring(1, len - 1);
			len = expr.length();
		}
		String regex = "[lookupOnChange|lookupNow].*\\((.+),(c.+)\\)\\)";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(expr);
		while (m.find()) {
			int from = m.start();
			int to = m.end();
			String prefix = "";
			String suffix = "";
			if (from > 0)
				prefix = expr.substring(0, from);
			String sub = expr.substring(from, to);
			if (to < len)
				suffix = expr.substring(to, len);
			// System.err.println("Lookup expr to process: " + expr + " = {\""
			// + prefix + "\"}.|." + sub + ".|.{\"" + suffix + "\"}");
			int idi = expr.indexOf("(");
			int com = expr.indexOf(",");
			String var = expr.substring(idi + 1, com);
			// System.err.println("\textracted variable [" + idi + "-" + com
			// + "]: " + var);
			boolean isBool = getExtVarType(var).equals(BOOL);
			expr = prefix + (isBool ? "b" : "i") + "lookup(" + var + ")"
					+ suffix;
		}
		return new String(expr);
	}

	public static String toSram(String oldExpr) {
		/* remove quote sign */
		String expr1 = oldExpr.replaceAll("'", "");
		/* replace const(v(x)) with c(x) */
		String expr2 = expr1.replaceAll("const\\(v\\((.+)\\)\\)", "c\\($1\\)");
		/* replace v(x) with c(x) */
		String expr3 = expr2.replaceAll("v\\((.+)\\)", "c\\($1\\)");
		/* replace LookupNow(v,t) with ilookup/blookup(v) */
		String expr4 = translateLookup(expr3);
		return expr4;
	}

	public static String getCondition(PlexilNodeCondition cond) {
		return toSram(cond.getExpression(nameMap));
	}

	public static String getConstraint(RandomValue r) {
		String constr;
		if (r != null) {
			Integer min = r.getMin();
			Integer max = r.getMax();
			if (min != null) {
				constr = "i(" + ivars + ") >= c(" + min + ")";
				if (max != null)
					constr += " ^ i(" + ivars + ") <= c(" + max + ")";
			} else {
				if (max != null)
					constr = "i(" + ivars + ") <= c(" + max + ")";
				else
					constr = "c(true)";
			}
		} else
			constr = "c(true)";
		return constr;
	}

	public static void processExternalVar(String evar, String evtype) {
		ArrayList<Integer> indices = new ArrayList<Integer>();
		ArrayList<String> constraints = new ArrayList<String>();
		boolean isBool = evtype.equals(BOOL);
		String prefix = (isBool ? "b" : "i");
		String constraint = new String();
		int vars = 0;
		for (PLEXILScript.Step step : script.getStep()) {
			boolean stutter = true;
			vars = (isBool ? bvars : ivars);
			for (PLEXILScript.Step.State state : step.getState()) {
				String shortname = new String(state.getName());
				if (evar.equals(shortname)) {
					stutter = false;
					indices.add(new Integer(vars));
					String v = state.getValue();
					RandomValue r = state.getRandomValue();
					if (v != null) {
						constraint = prefix + "(" + vars + ") === c(" + v + ")";
					} else {
						constraint = getConstraint(r);
					}
					constraints.add(new String(constraint));
					if (isBool)
						bvars++;
					else
						ivars++;
				}
			}
			if (stutter) {
				indices.add(new Integer(vars - 1));
				constraints.add(new String(constraint));
			}
		}
		sb.append("\n\n  eq " + evar + "-" + LOOKUP);
		sb.append(" = < " + evar + " : " + EXTVAR + " |\n");
		sb.append("\ttype |-> " + evtype + ", \n");
		sb.append("\tvalues |-> ");
		Integer i0 = indices.get(0);
		if (i0 != null) {
			sb.append(prefix + "(" + i0 + ")");
			indices.remove(0);
			for (Integer i : indices) {
				sb.append(" ; " + prefix + "(" + i + ")");
			}
		}
		sb.append(", \n");
		sb.append("\tconstraints |-> ");
		String cns0 = constraints.get(0);
		if (cns0 != null) {
			sb.append("(" + cns0 + ")");
			constraints.remove(0);
			for (String cns : constraints) {
				sb.append(" ; (" + cns + ")");
			}
		}
		sb.append(" > .\n");
	}

	public static void processExternalVars() {
		if (script == null)
			return;
		for (String evar : extvarNames) {
			String evtype = extnameMap.get(evar);
			processExternalVar(evar, evtype);
		}
	}

	public static void generateProperties() {
		if (checkForm) {
			sb.append("\n\nred " + (verbose ? "verify" : "verify-lite")
					+ "(init," + formula + ") .");
		}
		if (checkInv) {
			sb.append("\n\nred " + (verbose ? "verify" : "verify-lite")
					+ "(init, invs) .");
		}
		if (checkPre) {
			sb.append("\n\nred " + (verbose ? "verify" : "verify-lite")
					+ "(init, pres) .");
		}
		if (checkPost) {
			sb.append("\n\nred " + (verbose ? "verify" : "verify-lite")
					+ "(init, posts) .");
		}
		if (checkRace) {
			sb.append("\n\nred " + (verbose ? "verify" : "verify-lite")
					+ "(init, race-free) .");
		}

		// this option needs to be checked last!
		if (!interactive) {
			sb.append("\n q .");
		}
	}

	public static void plxc2sram(String source, String target) throws Exception {
		System.out.println("\t translating PLXC file " + source
				+ " to SRA Maude " + target);

		JAXBContext context = JAXBContext
				.newInstance("org.nianet.plexil.plexilxml2maude.jaxbmodel");
		Unmarshaller unmarshaller = context.createUnmarshaller();

		InputStream is = new FileInputStream(source);
		plan = (PlexilPlan) unmarshaller.unmarshal(is);

		// update nodes with variables and absolute names
		plan.fillOnNodesAbsolutePath();
		plan.setNodesLexicalScopedVariables();
		/*
		 * String maudeModule = plan.generateMaudeModule(null); BufferedWriter
		 * bw = new BufferedWriter(new FileWriter(target));
		 * bw.write(maudeModule); bw.close();
		 */

		/* initializations */
		nodeNames = new ArrayList<String>();
		varNames = new ArrayList<String>();
		getQualifiedNodeNames("", plan.getRootNode(), nodeNames);
		getQualifiedVariableNames("", plan.getRootNode(), varNames);
		allNames = new ArrayList<String>(nodeNames);
		allNames.addAll(varNames);
		initNameMapping(allNames);
		getExternalVariables();
		nameHash = new Hashtable<Node, String>();
		varHash = new Hashtable<DeclareVariable, String>();
		Node root = plan.getRootNode();
		collectAbsoluteNames(root, "");
		parentHash = new Hashtable<Node, Node>();
		collectParents(root, root);

		sb = new StringBuffer();
		declareFunctionalModule();
		declareNonfunctionalModule();
		generateProperties();
		sb.append("\n\neof");

		// System.out.println(sb.toString());

		FileWriter writer = new FileWriter(target);
		writer.write(sb.toString());
		writer.close();

	}
}
