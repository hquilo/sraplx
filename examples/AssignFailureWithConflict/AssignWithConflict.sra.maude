fmod ASSIGNWITHCONFLICT-SYNTAX is
  protecting STATE .
  --- node names and declarations
  ops AssignWithConflict A B : -> Name [ctor] .
  ops AssignWithConflict-node A-node B-node : -> Object .
  --- local variable names and declarations
  ops n : -> Name [ctor] .
  ops n-mem : -> Object .
  --- external variable names and declarations
  ops T : -> Name [ctor] .
  ops T-lookup : -> Object .

  eq AssignWithConflict-node
   = < AssignWithConflict : list |
	parent |-> nil, status |-> inactive, outcome |-> none,
	start |-> c(true), skip |-> c(false), end |-> children-finished(AssignWithConflict),
	repeat |-> c(false), pre |-> c(true), post |-> (imem(n . AssignWithConflict) === c(2)), inv |-> c(true) > .

  eq n-mem
   = < n . AssignWithConflict : mem |
	parent |-> AssignWithConflict, val |-> c(0), init-val |-> c(0) > .

  eq A-node
   = < A . AssignWithConflict : assg |
	parent |-> AssignWithConflict, status |-> inactive, outcome |-> none,
	start |-> (ilookup(T) >= c(0)), skip |-> c(false), end |-> c(true),
	repeat |-> c(false), pre |-> c(true), post |-> c(true), inv |-> c(true), 
	set(n . AssignWithConflict) |-> (imem(n . AssignWithConflict) + c(2)) > .

  eq B-node
   = < B . AssignWithConflict : assg |
	parent |-> AssignWithConflict, status |-> inactive, outcome |-> none,
	start |-> (ilookup(T) <= c(0)), skip |-> c(false), end |-> c(true),
	repeat |-> c(false), pre |-> c(true), post |-> c(true), inv |-> c(true), 
	set(n . AssignWithConflict) |-> (c(2)) > .


  eq T-lookup = < T : extvar |
	type |-> int, 
	values |-> i(0), 
	constraints |-> (i(0) >= c(1)) > .


endfm

mod ASSIGNWITHCONFLICT is
  protecting ASSIGNWITHCONFLICT-SYNTAX .
  protecting SRA-PLX .

  --- qualified name of this module
  eq USER-MODULE-NAME = 'ASSIGNWITHCONFLICT .

  --- initial state
  op init : -> Sys .
  eq init = { AssignWithConflict-node A-node B-node n-mem T-lookup } .
endm

red verify-lite(init, pres) .

red verify-lite(init, posts) .

red verify-lite(init, race-free) .
 q .

eof